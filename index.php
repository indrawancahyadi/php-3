<?php
    require_once('animal.php');
    require_once('Ape.php');
    require_once('Frog.php');
    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name."<br>"; // "shaun"
    echo "legs : ".$sheep->legs."<br>"; // 4
    echo "cold blooded : ".$sheep->cold_blooded."<br>"; // "no"
    echo "<br>";
    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>"; 
    echo "legs : ".$kodok->legs."<br>"; 
    echo "cold blooded : ".$kodok->cold_blooded."<br>";
    $kodok->jump(); // "hop hop"
    echo "<br>";
    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name."<br>"; 
    echo "legs : ".$sungokong->legs."<br>"; 
    echo "cold blooded : ".$sungokong->cold_blooded."<br>";
    $sungokong->yell(); 


?>